package com.phone

import com.phone.model.GBP
import com.phone.model.GBP._
import org.scalatest.{FlatSpec, Matchers}

import scala.util.{Failure, Try}

class MoneySpec extends FlatSpec with Matchers {
  "GBP" should "allow valid amounts" in {
    val gbp = GBP(100, 99)
    gbp.pounds shouldBe 100
    gbp.pence shouldBe 99
  }

  it should "verify constraints" in {
    Try(GBP(-2, 0)) shouldBe a[Failure[IllegalArgumentException]]
    Try(GBP(100, 101)) shouldBe a[Failure[IllegalArgumentException]]
    Try(GBP(0, -2)) shouldBe a[Failure[IllegalArgumentException]]
  }

  it should "add correctly" in {
    GBP(100, 0) + GBP(0, 99) shouldBe GBP(100, 99)
    GBP(100, 99) + GBP(0, 99) shouldBe GBP(101, 98)
  }

  it should "generate correctly from numbers" in {
    99.pence shouldBe GBP(0, 99)
    99.pounds shouldBe GBP(99, 0)
    199.pence shouldBe GBP(1, 99)
  }
}
