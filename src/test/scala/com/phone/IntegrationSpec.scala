package com.phone

import java.io.{File, PrintWriter}

import com.phone.services.{BillingService, PassThrough}
import org.scalatest.{FeatureSpec, GivenWhenThen, Matchers}

class IntegrationSpec extends FeatureSpec with GivenWhenThen with Matchers {
  private val billing = new BillingService with PassThrough

  private val validInput =
    """
      |A 555-333-212 00:02:03
      |A 555-433-242 00:06:41
      |A 555-433-242 00:01:03
      |B 555-333-212 00:01:20
      |A 555-333-212 00:01:10
      |A 555-663-111 00:02:09
      |A 555-333-212 00:04:28
      |B 555-334-789 00:00:03
      |A 555-663-111 00:02:03
      |B 555-334-789 00:00:53
      |B 555-971-219 00:09:51
      |B 555-333-212 00:02:03
      |B 555-333-212 00:04:31
      |B 555-334-789 00:01:59
    """.stripMargin

    private val validInputFile = File.createTempFile("test", "phone")
    validInputFile.deleteOnExit()
    new PrintWriter(validInputFile) {
      write(validInput)
      close()
    }

  feature("Billing") {
    scenario("Active promotion") {
      Given("The user provides valid file")
      val file = validInputFile

      When("The promotion is active")
      val flags = List.empty

      And("Billing is requested")
      val allArgs = List("--file", file.getAbsolutePath) ++ flags
      val output = Main.process(allArgs.toArray).mkString("\n")

      Then("Bills are correctly generated")
      val expectedOutput =
        """A: £ 0.32
          |B: £ 0.31""".stripMargin

      output shouldBe expectedOutput
    }

    scenario("Inactive promotion") {
      Given("The user provides valid file")
      val file = validInputFile

      When("The promotion is not active")
      val flags = List("--no-promotion")

      And("Billing is requested")
      val allArgs = List("--file", file.getAbsolutePath) ++ flags
      val output = Main.process(allArgs.toArray).mkString("\n")

      Then("Bills are correctly generated")
      val expectedOutput =
        """A: £ 0.53
          |B: £ 0.52""".stripMargin

      output shouldBe expectedOutput
    }
  }
}
