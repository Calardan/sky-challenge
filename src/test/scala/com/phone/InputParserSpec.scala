package com.phone

import com.phone.model.GBP._
import com.phone.model.{Bill, Call, GBP}
import com.phone.services.{BillingService, InputParser, PassThrough}
import org.scalatest.{FeatureSpec, FlatSpec, GivenWhenThen, Matchers}
import org.scalatest._

import scala.concurrent.duration._

class InputParserSpec  extends FlatSpec with Matchers {
  val billing = new BillingService with PassThrough

  "InputParser" should "parse valid input" in {
      val input = List(
        "A 555-333-212 00:02:03",
        "A 555-433-242 00:06:41",
        "A 555-433-242 00:01:03",
        "B 555-333-212 00:01:20",
        "A 555-333-212 00:01:10",
        "A 555-663-111 00:02:09",
        "A 555-333-212 00:04:28",
        "B 555-334-789 00:00:03",
        "A 555-663-111 00:02:03",
        "B 555-334-789 00:00:53",
        "B 555-971-219 00:09:51",
        "B 555-333-212 00:02:03",
        "B 555-333-212 00:04:31",
        "B 555-334-789 00:01:59")

        val calls = input.flatMap(InputParser.read)
        val expected = List(
          Call("A", "555-333-212", 123.seconds),
          Call("A", "555-433-242", 401.seconds),
          Call("A", "555-433-242", 63.seconds),
          Call("B", "555-333-212", 80.seconds),
          Call("A", "555-333-212", 70.seconds),
          Call("A", "555-663-111", 129.seconds),
          Call("A", "555-333-212", 268.seconds),
          Call("B", "555-334-789", 3.seconds),
          Call("A", "555-663-111", 123.seconds),
          Call("B", "555-334-789", 53.seconds),
          Call("B", "555-971-219", 591.seconds),
          Call("B", "555-333-212", 123.seconds),
          Call("B", "555-333-212", 271.seconds),
          Call("B", "555-334-789", 119.seconds)
        )

      calls shouldBe expected
    }

   it should "reject invalid input" in {
      val input = List(
        "A 555-433-242x 00:06:41",
        "A* 555-433-242 00:01:03",
        "A555-433-242 00:01:03",
        "555-433-242 00:01:03",
        "A 00:01:03",
        "A 555-433-242")

      val calls = input.flatMap(InputParser.read)
      calls shouldBe List()
   }
}
