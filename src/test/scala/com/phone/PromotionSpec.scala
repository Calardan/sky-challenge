package com.phone

import com.phone.model.{Bill, Call, GBP}
import com.phone.services.{BillingService, Promotion}
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._

class PromotionSpec extends FlatSpec with Matchers {
  val billing = new BillingService with Promotion

  "BillingService with Promotion" should "generate bills for a single customer" in {
    val calls = List(
      Call("1", "01558-926333", 4.minutes),
      Call("1", "07451-509467", 5.minutes),
      Call("1", "07451-509467", 12.minutes),
      Call("1", "07626-130853", 1.hours)
    )

    val billings = billing.request(calls)
    val expected = List(Bill("1", GBP(0, 49)))

    billings shouldBe expected
  }

  it should "generate bills for multiple for a single customer (2)" in {

    val calls = List(
      Call("1", "01558-926333", 4.minutes),
      Call("1", "07451-509467", 5.minutes),
      Call("1", "07626-130853", 12.minutes),
      Call("1", "07626-130853", 1.hours))

    val billings = billing.request(calls)
    val expected = List(Bill("1", GBP(0, 24)))

    billings shouldBe expected
  }

  it should "generate bills for 4 calls for 3 customers" in {
    val calls = List(
      Call("1", "01558-926333", 4.minutes),
      Call("2", "07451-509467", 5.minutes),
      Call("3", "07626-130853", 1.hours),
      Call("3", "07626-130853", 2.hours))

    val billings = billing.request(calls)
    val expected = List(
      Bill("1", GBP(0, 0)),
      Bill("2", GBP(0, 0)),
      Bill("3", GBP(0, 0))
    )

    billings shouldBe expected
  }
}
