package com.phone

import com.phone.model.GBP._
import com.phone.model.{Bill, Call, GBP}
import com.phone.services.{BillingService, PassThrough}
import org.scalatest.{FlatSpec, Matchers}

import scala.concurrent.duration._

class BillingServiceSpec extends FlatSpec with Matchers {
  val billing = new BillingService with PassThrough

  "Billing" should "calculate bills for short cases" in {
    val calls = List(
      Call("1", "01558-926333", 3.minutes),
      Call("1", "07451-509467", 2.minutes),
      Call("1", "07626-130853", 1.minutes))

    val billings = billing.request(calls)
    val expected = List(Bill("1", 18.pence))

    billings shouldBe expected
  }

  it should "calculate bills for longer calls" in {
    val calls = List(
      Call("1", "01558-926333", 4.minutes),
      Call("1", "07451-509467", 5.minutes),
      Call("1", "07626-130853", 1.hours))

    val billings = billing.request(calls)
    val expected = List(Bill("1", GBP(1, 35)))
    billings shouldBe expected
  }

  it should "correctly calculate bills for multiple customers" in {
    val calls = List(
      Call("1", "01558-926333", 4.minutes),
      Call("2", "07451-509467", 5.minutes),
      Call("3", "07626-130853", 1.hours),
      Call("3", "07626-130853", 2.hours)
    )

    val billings = billing.request(calls)
    val expected = List(
      Bill("1", GBP(0, 11)),
      Bill("2", GBP(0, 13)),
      Bill("3", GBP(3, 32)))

    billings shouldBe expected
  }
}
