package com.phone.model

case class Bill(customerId: String, amount: Money)
