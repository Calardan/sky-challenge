package com.phone.model

sealed trait Money {

}
case class GBP(pounds: Int, pence: Int) extends Money {
  require(pounds >= 0)
  require(pence >= 0)
  require(pence < 100)

  def +(amount: GBP): GBP = {
    val totalPence = this.pence + amount.pence
    val totalPounds = this.pounds + amount.pounds
    GBP(totalPounds + totalPence / 100, totalPence % 100)
  }

  override def toString: String = s"£ $pounds.$pence"
}

object GBP {
  implicit class IntWrapper(amount: Int) {
    def pounds: GBP = GBP(amount, 0)
    def pence: GBP = GBP(amount / 100, amount % 100)
  }
}




