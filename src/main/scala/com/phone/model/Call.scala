package com.phone.model

import com.phone.model.Customer.{CustomerId, PhoneNumber}

import scala.concurrent.duration.Duration

object Customer {
  type CustomerId = String
  type PhoneNumber = String
}

case class Call(customerId: CustomerId, number: PhoneNumber, duration: Duration)
