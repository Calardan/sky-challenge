package com.phone

import java.io.File

import com.phone.Main.args
import com.phone.services.{BillingService, InputParser, PassThrough, Promotion}
import org.rogach.scallop.{ScallopConf, ScallopOption}

import scala.io.Source

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val file: ScallopOption[String] = opt[String](required = true)
  val noPromotion: ScallopOption[Boolean] = opt[Boolean](required = false, default = Some(false))
  verify()
}

object Main extends App {
  def process(args: Array[String]): Seq[String] = {
    val conf = new Conf(args)
    val file = conf.file()

    if (!new File(file).isFile) {
      Seq(s"File $file does not exist or is not a file")
    } else {
      val lines = Source.fromFile(file).getLines().toSeq

      val calls = lines.flatMap(InputParser.read)

      val billing = if (!conf.noPromotion()) {
        new BillingService with Promotion
      } else {
        new BillingService with PassThrough
      }

      val bills = billing.request(calls)

      bills.map { bill =>
        s"${bill.customerId}: ${bill.amount}"
      }
    }
  }

  val output = process(args)

  output.foreach(println)
}
