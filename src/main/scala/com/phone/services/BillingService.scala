package com.phone.services

import com.phone.model.{Bill, Call, Money}
import com.phone.model.GBP._
import com.phone.services.BillingService.CallFilter

object BillingService {
  type CallFilter = Seq[(Call, Double)] => Seq[(Call, Double)]
}

/*
 Billing service which generates bills from calls.
 A CallFilter must be provided
 */
abstract class BillingService {
  val callFilter: CallFilter

  val REDUCED_PRICE = 0.03
  val FULL_PRICE = 0.05
  val FULL_PRICE_MINUTES = 180

  /**
    * Generate bills from calls
    * @param calls input calls
    * @return generated list of bills per customer
    */
  def request(calls: Seq[Call]): Seq[Bill] = {
    calls.groupBy(_.customerId).toSeq.map {
      case (customerId, customerCalls) =>
        requestForCustomer(customerId, customerCalls)
    }
  }

  private def requestForCustomer(customerId: String, calls: Seq[Call]): Bill = {
    val callsAndPrices  = calls.map {
      customerCall =>
        val totalSeconds = customerCall.duration.toSeconds
        val fullPrizeSeconds = Math.min(FULL_PRICE_MINUTES, totalSeconds)
        val reducedSeconds = Math.max(totalSeconds-FULL_PRICE_MINUTES, 0)

        (customerCall, fullPrizeSeconds * FULL_PRICE + reducedSeconds * REDUCED_PRICE)
    }

    val filtered = callFilter(callsAndPrices)
    val totalPrice = filtered.map(_._2).foldLeft(0.0)(_ + _)

    Bill(customerId, totalPrice.ceil.toInt.pence)
  }
}

/**
  * Accept all calls for all customers
  */
trait PassThrough {
  val callFilter: CallFilter = callsAndPrices => callsAndPrices
}

/**
  * Filter most costly number for each customer
  */
trait Promotion {
  val callFilter: CallFilter =
    callsAndPrices => {
      val groupedByNumber = callsAndPrices.groupBy(_._1.number).toSeq
      val withPrices = groupedByNumber.map {
        case (number, customerCalls) =>
          val totalPrice = customerCalls.map(_._2).foldLeft(0.0){ _ + _}
          (number, totalPrice)
      }

      val max = withPrices.maxBy(_._2)
      val filtered = callsAndPrices.filterNot(_._1.number == max._1)

      filtered
    }
}
