package com.phone.services

import com.phone.model.Call

import scala.concurrent.duration._
import scala.util.parsing.combinator._


private object CallParser extends RegexParsers {
  override def skipWhitespace: Boolean = false

  def customerId: Parser[String] =
    """[a-zA-Z1-9]+""".r ^^ { _.toString }

  def number: Parser[String] =
    """([1-9\\-]+)""".r ^^ { _.toString }

  def timeSegment: Parser[Int] =
    """([0-9]{2})""".r ^^ { _.toInt }

  def duration: Parser[Duration]
    = timeSegment ~ ":" ~ timeSegment ~ ":" ~ timeSegment ^^
      { case h ~ ":" ~ m ~ ":" ~ s => (h * 360 + m * 60 + s).seconds }

  def call: Parser[Call]
    = customerId ~ whiteSpace ~ number ~ whiteSpace ~ duration ^^
      { case c ~ w1 ~ n ~ w2 ~ d => Call(c, n, d) }
}

/**
  Parse input lines into Call objects
  */
object InputParser {
  def read(line: String): Option[Call] = {
    CallParser.parse(CallParser.call, line) match {
      case CallParser.Success(call, _) => Some(call)
      case _ => None
    }
  }
}
